//Set variables
var filter = null;
var sort = null;
var num = 0;

//FCKEditor function
function SelectFile( fileUrl, width, height, alt )
{
  // window.opener.SetUrl( url, width, height, alt);
  window.opener.SetUrl( fileUrl, null, null, alt ) ;
  window.close();
}

//Prepare the links in the browser
function prepareLinks() {
  //Add on click functions for images
  $("#browse .browse ul.images li a").click(function() {
    $("#insert .details").html('<p class="message">Loading...</p>');
    var href = $(this).attr("href");
    $("#browse .browse ul.images li").css('border-color', 'transparent');
    $("#browse .browse ul.images li").css('background-color', '#FFFFFF');
    $(this).parent().css('border-color', '#E5E58F');
    $(this).parent().css('background-color', '#FCFCE8');
    $("#insert .details").load(href);
    return false;
  });
  
  //Add on click functions for pager
  $("#browse .browse ul.pager li a").click(function() {
    $("#browse .browse").html('<ul class="images"><li class="message">Loading...</li></ul><div class="item-list"><ul class="pager"><li>-</li></ul></div>');
    var href = $(this).attr("href");
    var parts = href.split("=");
    $("#browse .browse").load('/imagebrowser/load/images/'+filter+'/'+sort+'?page='+parts[1], function() {
      prepareLinks(this);
    });
    return false;
  });
}

//Sort things out when the page loads
$(document).ready(function(){
  //Load on filter selection change
  $("#filter").change(function () {
    filter = $("#filter option:selected").attr('value');
    $("#browse .browse").html('<ul class="images"><li class="message">Loading...</li></ul><div class="item-list"><ul class="pager"><li>-</li></ul></div>');
    $("#browse .browse").load('/imagebrowser/load/images/'+filter+'/'+sort, function() {
      prepareLinks(this);
    });
  }).change();
  
  //Load on sort selection change
  $("#sort").change(function () {
    sort = $("#sort option:selected").attr('value');
    $("#browse .browse").html('<ul class="images"><li class="message">Loading...</li></ul><div class="item-list"><ul class="pager"><li>-</li></ul></div>');
    $("#browse .browse").load('/imagebrowser/load/images/'+filter+'/'+sort, function() {
      prepareLinks(this);
    });
  }).change();
  
  //Load first set of images on page load
  $("#browse .browse").load('/imagebrowser/load/images/'+filter+'/'+sort, function() {
    prepareLinks(this);
  });
});