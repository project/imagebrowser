==============================
Installation
==============================

1. Drop these files and folder into the modules folder (/sites/all/modules/)
2. Enable ImageBrowser module (?q=/admin/build/modules)

Required Modules:
------------------------------

 - Image API (http://drupal.org/project/imageapi)
 - Image (http://drupal.org/project/image)

Optional (but recommended) Modules:
------------------------------

 - FCKeditor (http://drupal.org/project/fckeditor) - Note: While Image Browser will work without FCKeditor you won't be able to insert the images into your nodes without it.
 - ImageCache (http://drupal.org/project/imagecache)

==============================
Setup after install
==============================

 - If using ImageCache (2.0-beta1) then I have had to apply this patch (#326281) to get PNGs to display properly.
 - To get Image Browser working with FCKeditor (1.3-rc3) then you will need to apply this patch (#327615).
 - Go into the FCKeditor admin (?q=/admin/settings/fckeditor), edit a profile and under 'File browser settings' select 'Image Browser' as the File browser type.
 - Make sure you have the option "Image" in your toolbar.
 - Admin options are available here: ?q=/admin/setting/imagebrowser

==============================
Using
==============================

 - Go to create a node where FCKeditor is available.
 - Click on the image icon in the toolbar.
 - Click on 'Browse Server'
 - Here you can either upload a new image or select an existing image. Once selected you will be presented with a list of 'Image' and 'ImageCache' presets that you can insert.
 
==============================
The Future  
==============================

If you have any questions, issues, or feature suggestions then please do leave feedback on the project page (http://drupal.org/project/imagebrowser)